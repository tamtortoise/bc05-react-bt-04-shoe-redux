
import Cart from "./Cart";
import DetailShoe from "./DetailShoe";
import ListShoe from "./ListShoe";
import React, { Component } from 'react'

export default class ExShoeRedux extends Component {
  render() {
    return (
      <div className="container">
        <Cart />
        <ListShoe />
        <DetailShoe />
      </div>
    )
  }
}




