import { dataShoe } from "../../dataShoe";
import { ADD_TO_CART, CHANGE_QTY, VIEW_DETAIL } from "../constant/constant";

let initialState = {
  shoeArr: dataShoe,
  detail: dataShoe[0],
  cart: [],
};

export const shoeReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART: {
      let cloneCart = [...state.cart];

      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload.id;
      });
      if (index === -1) {
        let cartItem = { ...action.payload, number: 1 };
        cloneCart.push(cartItem);
      } else {
        cloneCart[index].number++;
      }
      return { ...state, cart: cloneCart };
    }
    case CHANGE_QTY: {
      let cloneCart = [...state.cart];
      let index = cloneCart.findIndex((item) => {
        return item.id === action.payload;
      });

      if (cloneCart[index].number === 1 && action.value === -1) {
        cloneCart.splice(index, 1);
      } else {
        cloneCart[index].number += action.value;
      }

      return { ...state, cart: cloneCart };
    }
    case VIEW_DETAIL: {
      return { ...state, detail: action.payload };
    }
    default:
      return state;
  }
};
